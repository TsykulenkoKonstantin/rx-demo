package com.tsykul.rx.examples.netty.logtail;

import io.netty.buffer.ByteBuf;
import io.netty.handler.logging.LogLevel;
import io.reactivex.netty.RxNetty;
import io.reactivex.netty.pipeline.PipelineConfigurators;
import io.reactivex.netty.protocol.http.client.HttpClient;
import io.reactivex.netty.protocol.http.client.HttpClientRequest;
import io.reactivex.netty.protocol.http.server.HttpServer;
import io.reactivex.netty.protocol.http.server.HttpServerRequest;
import io.reactivex.netty.protocol.http.server.HttpServerResponse;
import io.reactivex.netty.protocol.http.sse.ServerSentEvent;
import rx.Observable;

import java.util.ArrayList;
import java.util.List;

public class LogAggregator {

    static final int DEFAULT_AG_PORT = 8091;

    private final int port;
    private final int producerPortFrom;
    private final int producerPortTo;
    HttpServer<ByteBuf, ServerSentEvent> server;

    public LogAggregator(int port, int producerPortFrom, int producerPortTo) {
        this.port = port;
        this.producerPortFrom = producerPortFrom;
        this.producerPortTo = producerPortTo;
    }

    public HttpServer<ByteBuf, ServerSentEvent> createAggregationServer() {
        server = RxNetty.newHttpServerBuilder(port,
                (HttpServerRequest<ByteBuf> request,
                 HttpServerResponse<ServerSentEvent> response) ->
                        connectToLogProducers().flatMap(response::writeAndFlush))
                .enableWireLogging(LogLevel.ERROR)
                .pipelineConfigurator(PipelineConfigurators.<ByteBuf>serveSseConfigurator())
                .build();
        System.out.println("Logs aggregator server started...");
        return server;
    }

    private Observable<ServerSentEvent> connectToLogProducers() {
        List<Observable<ServerSentEvent>> oList = new ArrayList<>(producerPortTo - producerPortFrom + 1);
        for (int i = producerPortFrom; i <= producerPortTo; i++) {
            oList.add(connectToLogProducer(i));
        }
        return Observable.merge(oList);
    }

    private static Observable<ServerSentEvent> connectToLogProducer(int port) {
        HttpClient<ByteBuf, ServerSentEvent> client =
                RxNetty.createHttpClient("localhost", port, PipelineConfigurators.<ByteBuf>clientSseConfigurator());

        return client.submit(HttpClientRequest.createGet("/logstream")).flatMap(response -> response.getContent()
                       .doOnNext(ServerSentEvent::retain));
    }

    public static void main(final String[] args) {
        if (args.length < 2) {
            System.err.println("ERROR: provide log producers port range");
            return;
        }
        int producerPortFrom = Integer.valueOf(args[0]);
        int producerPortTo = Integer.valueOf(args[1]);
        LogAggregator aggregator = new LogAggregator(DEFAULT_AG_PORT, producerPortFrom, producerPortTo);
        aggregator.createAggregationServer().startAndWait();
        System.out.println("Aggregator service terminated");
    }
}
