package com.tsykul.rx.examples.netty.logtail;

import io.netty.buffer.ByteBuf;
import io.netty.handler.logging.LogLevel;
import io.reactivex.netty.RxNetty;
import io.reactivex.netty.pipeline.PipelineConfigurators;
import io.reactivex.netty.protocol.http.server.HttpServer;
import io.reactivex.netty.protocol.http.server.HttpServerRequest;
import io.reactivex.netty.protocol.http.server.HttpServerResponse;
import io.reactivex.netty.protocol.http.sse.ServerSentEvent;
import rx.Observable;

import java.util.concurrent.TimeUnit;

public class LogProducer {

    private final int port;
    private final long interval;
    private final String source;

    public LogProducer(int port, int interval) {
        this.port = port;
        this.interval = interval;
        source = "localhost:" + port;
    }

    public HttpServer<ByteBuf, ServerSentEvent> createServer() {
        HttpServer<ByteBuf, ServerSentEvent> server = RxNetty.newHttpServerBuilder(port,
                (HttpServerRequest<ByteBuf> request, HttpServerResponse<ServerSentEvent> response) ->
                        createReplyHandlerObservable(response))
                .pipelineConfigurator(PipelineConfigurators.<ByteBuf>serveSseConfigurator())
                .enableWireLogging(LogLevel.DEBUG)
                .build();
        System.out.println("Started log producer on port " + port);
        return server;
    }

    private Observable<Void> createReplyHandlerObservable(final HttpServerResponse<ServerSentEvent> response) {
        return Observable.interval(interval, TimeUnit.MILLISECONDS)
                .flatMap(interval1 -> {
                    ByteBuf eventId = response.getAllocator().buffer().writeLong(interval1);
                    ByteBuf data = response.getAllocator().buffer().writeBytes(LogEvent.randomLogEvent(
                            source).toCSV().getBytes());
                    return response.writeAndFlush(ServerSentEvent.withEventId(eventId, data));
                });
    }

    public static void main(final String[] args) {
        if (args.length < 2) {
            System.err.println("ERROR: specify log producer's port number and a message sending interval");
            return;
        }
        int port = Integer.valueOf(args[0]);
        int interval = Integer.valueOf(args[1]);
        new LogProducer(port, interval).createServer().startAndWait();
        System.out.println("LogProducer service terminated");
    }

}
