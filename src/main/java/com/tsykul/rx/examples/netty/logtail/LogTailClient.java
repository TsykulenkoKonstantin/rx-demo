package com.tsykul.rx.examples.netty.logtail;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.logging.LogLevel;
import io.reactivex.netty.RxNetty;
import io.reactivex.netty.pipeline.PipelineConfigurators;
import io.reactivex.netty.protocol.http.client.HttpClient;
import io.reactivex.netty.protocol.http.client.HttpClientRequest;
import io.reactivex.netty.protocol.http.sse.ServerSentEvent;
import rx.Observable;

import java.util.ArrayList;
import java.util.List;

public class LogTailClient {

    static final int DEFAULT_TAIL_SIZE = 25;

    private final int port;
    private final int tailSize;

    public LogTailClient(int port, int tailSize) {
        this.port = port;
        this.tailSize = tailSize;
    }

    public List<LogEvent> collectEventLogs() {
        HttpClient<ByteBuf, ServerSentEvent> client =
                RxNetty.<ByteBuf, ServerSentEvent>newHttpClientBuilder("localhost", port)
                        .enableWireLogging(LogLevel.DEBUG)
                        .pipelineConfigurator(PipelineConfigurators.<ByteBuf>clientSseConfigurator()).build();

        Iterable<LogEvent> eventIterable = client.submit(HttpClientRequest.createGet("/logstream"))
                .flatMap(response -> {
                    if (response.getStatus().equals(HttpResponseStatus.OK)) {
                        return response.getContent();
                    }
                    return Observable.error(new IllegalStateException("server returned status " + response.getStatus()));
                }).map(serverSentEvent -> LogEvent.fromCSV(serverSentEvent.contentAsString())
                ).filter(logEvent -> logEvent.getLevel() == LogEvent.LogLevel.ERROR
                ).take(tailSize).toBlocking().toIterable();

        List<LogEvent> logs = new ArrayList<>();
        for (LogEvent e : eventIterable) {
            System.out.println("event " + logs.size() + ": " + e);
            logs.add(e);
        }
        return logs;
    }

    public static void main(String[] args) {
        LogTailClient client = new LogTailClient(LogAggregator.DEFAULT_AG_PORT, DEFAULT_TAIL_SIZE);
        List<LogEvent> logEvents = client.collectEventLogs();
        System.out.printf("LogTailClient service collected %d entries", logEvents.size());
    }
}
