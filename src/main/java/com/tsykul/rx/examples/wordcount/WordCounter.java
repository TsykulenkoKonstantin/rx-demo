package com.tsykul.rx.examples.wordcount;

import rx.Observable;
import rx.observables.GroupedObservable;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class WordCounter {

    private final String[] strings = new String[]{
            "There is no spoon",
            "Some stuff",
            "Funny stuff"
    };
    private final Random random = new Random();

    public static void main(String... args) {
        WordCounter wordCounter = new WordCounter();
        wordCounter.run();
    }

    private void run() {
        Observable<Long> intervals = Observable.interval(100, TimeUnit.MILLISECONDS);
//        intervals.subscribe(System.out::println);
        Observable<String> strings = intervals.map(aLong -> getString());
//        strings.subscribe(System.out::println);
        Observable<String> words = strings.flatMap(s -> Observable.from(s.split(" ")));
//        words.subscribe(System.out::println);
        Observable<String> noFWords = words.filter(w -> !w.toLowerCase().startsWith("f"));
//        noFWords.subscribe(System.out::println);
        Observable<GroupedObservable<String, String>> groupedByWords = noFWords.groupBy(s -> s);

        groupedByWords.forEach(wordObservable -> {
//            wordObservable.subscribe(s -> System.out.println(wordObservable.getKey() + ":" + s));
            Observable<Long> wordCounts = wordObservable.buffer(5, 5, TimeUnit.SECONDS).map(l -> l.stream().count());
//            wordCounts.subscribe(System.out::println);
            final String key = wordObservable.getKey();
            Observable<WordCount> wordsAndCounts = wordCounts.map(c -> new WordCount(key, c));
//            wordsAndCounts.subscribe(System.out::println);
        });

        //end result
        strings
                .flatMap(s -> Observable.from(s.split(" ")))
                .filter(w -> !w.toLowerCase().startsWith("f"))
                .groupBy(s -> s)
                .forEach(wordObservable -> {
                    wordObservable
                            .buffer(5, 5, TimeUnit.SECONDS)
                            .map(l -> new WordCount(wordObservable.getKey(), l.stream().count()))
                            .subscribe(System.out::println);
                });

        intervals.toBlocking().last();
    }

    private String getString() {
        return strings[random.nextInt(strings.length)];
    }

    private static final class WordCount {
        private final String word;
        private final long count;

        private WordCount(String word, long count) {
            this.word = word;
            this.count = count;
        }

        public String getWord() {
            return word;
        }

        public long getCount() {
            return count;
        }

        @Override
        public String toString() {
            return "word='" + word + '\'' +
                    ", count=" + count;
        }
    }
}
